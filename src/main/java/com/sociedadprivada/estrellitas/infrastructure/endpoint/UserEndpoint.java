package com.sociedadprivada.estrellitas.infrastructure.endpoint;

import com.sociedadprivada.estrellitas.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

@Endpoint
public class UserEndpoint {

    @Autowired
    private UserService userService;


    @PayloadRoot(namespace = "http://lasestrellitas.com/example",
            localPart = "getUserRequest")
    @ResponsePayload
    public com.sociedadprivada.estrellitas.infrastructure.generate.GetUserResponse getUserRequest(@RequestPayload com.sociedadprivada.estrellitas.infrastructure.generate.GetUserRequest request) {
        com.sociedadprivada.estrellitas.infrastructure.generate.GetUserResponse response = new com.sociedadprivada.estrellitas.infrastructure.generate.GetUserResponse();
        response.setUser(userService.getUsers(request.getName()));
        return response;
    }
}
