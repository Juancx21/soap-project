package com.sociedadprivada.estrellitas;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EstrellitasApplication {

	public static void main(String[] args) {
		SpringApplication.run(EstrellitasApplication.class, args);
	}

}
